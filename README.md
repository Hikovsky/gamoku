Игра "Гомоку" на C# с использованием алгоритма "МиниМакс" для реализации игры "Компютер vs Компютер"

Информация:
- Использованный алгоритм: МиниМакс
- Язык программирования: C#
- Написал: Hikovsky

Gomoku game in C # using MiniMax algorithm to implement Computer vs Computer game.

Information:
- Used algorithm: MiniMax
- Programming language: C #
- Author Hikovsky
