﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics.Tracing;
using System.Linq;
using System.Net;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices.ComTypes;
using System.Security.Cryptography.X509Certificates;

namespace Gomoku
{
    class Program
    {
        public static int winScore = 99999999;

        // Создание и заполнение поля
        public static char[,] CreatePlayground()
        {
            char[,] playground = new char[15, 15];
            for (int i = 0; i < playground.GetLength(0); i++)
            {
                for (int j = 0; j < playground.GetLength(1); j++)
                {
                    playground[j, i] = '_';
                }
            }
            return playground;
        }

        // Вывод поля в командную строку
        public static void DrawPlayground(char[,] playground)
        {
            Console.Clear(); // Очищаем консоль
            Console.WriteLine("  00 01 02 03 04 05 06 07 08 09 10 11 12 13 14");
            for (int i = 0; i < playground.GetLength(0); i++)
            {
                Console.Write($"{i:d2}");
                for (int j = 0; j < playground.GetLength(1); j++)
                {
                    Console.Write($" {playground[i, j]} ");
                }

                Console.WriteLine();
            }
        }

        public static char[,] CopyPlayground(char[,] playground)
        {
            char[,] result = new char[15, 15];
            for (int i = 0; i < playground.GetLength(0); i++)
            {
                for (int j = 0; j < playground.GetLength(1); j++)
                {
                    result[i, j] = playground[i, j];
                }
            }
            return result;
        }

        // Список возможных ходов
        public static List<int[]> GenerateMoves(char[,] playground)
        {
            List<int[]> possibleMoves = new List<int[]>();

            for (int i = 1; i < playground.GetLength(0) - 1; i++)
            {
                for (int j = 1; j < playground.GetLength(1) - 1; j++)
                {
                    if (playground[i, j] != '_')
                        continue;
                    if (playground[i - 1, j - 1] != '_' || playground[i - 1, j] != '_' || playground[i - 1, j + 1] != '_' ||
                        playground[i, j - 1] != '_' || playground[i, j + 1] != '_' ||
                        playground[i + 1, j - 1] != '_' || playground[i + 1, j] != '_' || playground[i + 1, j + 1] != '_')
                    {
                        int[] possibleMove = { i, j };
                        possibleMoves.Add(possibleMove);
                        continue;
                    }
                }
            }

            if (possibleMoves.Count == 0)
                possibleMoves.Add(new int[] { 7, 7 });
            return possibleMoves;
        }

        public static int[][] GetCoordinates(char[,] playground, char player)
        {
            List<int[]> result = new List<int[]>();
            for (int i = 0; i < playground.GetLength(0); i++)
            {
                for (int j = 0; j < playground.GetLength(1); j++)
                {
                    if (playground[i, j] == player)
                        result.Add(new int[] { i, j });
                }
            }
            return result.ToArray();
        }

        public static bool CheckForWin(char[,] playground, char player)
        {
            int max_right = 1;
            int max_down = 1;
            int max_right_down = 1;
            int max_left_down = 1;
            int[][] coordinates = GetCoordinates(playground, player);
            for (int i = 0; i < coordinates.Length; i++)
            {
                if (i == coordinates.Length - 1)
                    break;
                int x = coordinates[i][0];
                int y = coordinates[i][1];
                int right = 1;
                int down = 1;
                int right_down = 1;
                int left_down = 1;
                for (int j = i + 1; j < coordinates.Length; j++)
                {
                    // Check right
                    if (x == coordinates[j][0] && y == coordinates[j][1] - right)
                    {
                        if (right != 5)
                            right++;
                        if (max_right < right)
                            max_right = right;
                    }
                    // Check down
                    else if (x == coordinates[j][0] - down && y == coordinates[j][1])
                    {
                        if (down != 5)
                            down++;
                        if (max_down < down)
                            max_down = down;
                    }
                    // Check right down
                    else if (x == coordinates[j][0] - right_down && y == coordinates[j][1] - right_down)
                    {
                        if (right_down != 5)
                            right_down++;
                        if (max_right_down < right_down)
                            max_right_down = right_down;
                    }
                    // Check left down
                    else if (x == coordinates[j][0] - left_down && y == coordinates[j][1] + left_down)
                    {
                        if (left_down != 5)
                            left_down++;
                        if (max_left_down < left_down)
                            max_left_down = left_down;
                    }
                }
            }
            int max = Math.Max(Math.Max(max_right, max_down), Math.Max(max_right_down, max_left_down));
            if (max >=5)
            {
                Console.WriteLine($"Plyaer {player} win");
                return false;
            }
            return true;
        }

        public static int CalculateHorizontal(int[][] coordinates)
        {
            double result = 0;
            for (int i = 0; i < coordinates.Length - 1; i++)
            {
                int right = 0;
                int row = coordinates[i][0];
                int column = coordinates[i][1];
                for (int j = 0; j < coordinates.Length; j++)
                {
                    if (row == coordinates[j][0] && column == coordinates[j][1] - right)
                    {
                        right++;
                        switch (right)
                        {
                            case 1:
                                result += 10;
                                break;
                            case 2:
                                result += 100;
                                break;
                            case 3:
                                result += 500;
                                break;
                            case 4:
                                result += 1000;
                                break;
                            case 5:
                                return winScore;
                        }
                    }
                }

            }
            return (int)result;
        }

        public static int CalculateVertical(int[][] coordinates)
        {
            double result = 0;
            for (int i = 0; i < coordinates.Length - 1; i++)
            {
                int down = 0;
                int row = coordinates[i][0];
                int column = coordinates[i][1];
                for (int j = 0; j < coordinates.Length; j++)
                {
                    if (row == coordinates[j][0] - down && column == coordinates[j][1])
                    {
                        down++;
                        switch (down)
                        {
                            case 1:
                                result += 10;
                                break;
                            case 2:
                                result += 100;
                                break;
                            case 3:
                                result += 500;
                                break;
                            case 4:
                                result += 1000;
                                break;
                            case 5:
                                return winScore;
                        }
                    }
                }
            }
            return (int)result;
        }

        public static int CalculateDiagonal(int[][] coordinates)
        {
            double result = 0;
            for (int i = 0; i < coordinates.Length - 1; i++)
            {
                int rightDown = 0;
                int leftDown = 0;
                int row = coordinates[i][0];
                int column = coordinates[i][1];
                for (int j = 0; j < coordinates.Length; j++)
                {
                    if (row == coordinates[j][0] - rightDown && column == coordinates[j][1] - rightDown)
                    {
                        rightDown++;
                        switch (rightDown)
                        {
                            case 1:
                                result += 10;
                                break;
                            case 2:
                                result += 100;
                                break;
                            case 3:
                                result += 500;
                                break;
                            case 4:
                                result += 1000;
                                break;
                            case 5:
                                return winScore;
                        }
                    }

                    if (row == coordinates[j][0] - rightDown && column == coordinates[j][1] + leftDown)
                    {
                        leftDown++;
                        switch (leftDown)
                        {
                            case 1:
                                result += 10;
                                break;
                            case 2:
                                result += 100;
                                break;
                            case 3:
                                result += 500;
                                break;
                            case 4:
                                result += 1000;
                                break;
                            case 5:
                                return winScore;
                        }
                    }
                }
            }
            return (int)result;
        }

        public static int CalculateScore(char[,] playground, char player)
        {
            int[][] coordinates = GetCoordinates(playground, player);
            int horizontal = CalculateHorizontal(coordinates);
            int vertical = CalculateVertical(coordinates);
            int diagonal = CalculateDiagonal(coordinates);
            return horizontal + vertical + diagonal;
        }

        public static int[] BestMove(char[,] playground, char player)
        {
            int[] resultPlayer = new int[2];
            int[] resultOpponent = new int[2];
            int maxScorePlayer = 0;
            int maxScoreOpponent = 0;
            List<int[]> possibleMoves = GenerateMoves(playground);
            char temp = player == 'x' ? 'o' : 'x';
            foreach (int[] move in possibleMoves)
            {
                char[,] newPlayGround = CopyPlayground(playground);
                newPlayGround[move[0], move[1]] = player;
                int score = CalculateScore(newPlayGround, player);
                if (score >= maxScorePlayer)
                {
                    resultPlayer = move;
                    maxScorePlayer = score;
                }
            }
            foreach (int[] move in possibleMoves)
            {
                char[,] newPlayGround = CopyPlayground(playground);
                newPlayGround[move[0], move[1]] = temp;
                int score = CalculateScore(newPlayGround, temp);
                if (score >= maxScoreOpponent)
                {
                    resultOpponent = move;
                    maxScoreOpponent = score;
                }
            }
            if (maxScorePlayer > maxScoreOpponent)
                return resultPlayer;
            else
                return resultOpponent;
        }

        public static char[,] MakeMove(char[,] playground, char player)
        {
            int[] bestMove = BestMove(playground, player);
            playground[bestMove[0], bestMove[1]] = player;
            return playground;
        }

        static void Main(string[] args)
        {
            char[,] playground = CreatePlayground();
            bool theGameContinues = true;
            int i = 0;
            while (theGameContinues)
            {
                char player = i % 2 == 0 ? 'o' : 'x';
                playground = MakeMove(playground, player);
                i++;
                DrawPlayground(playground);
                theGameContinues = CheckForWin(playground, player);             
            }
        }
    }
}
